#!/bin/bash
if [ $# -ne 1 ]
then
    echo "Syntaxe : $0 ./usfm (exemple : $0 ./usfm )"
    exit 1
fi
if [ ! -d "$1" ]
then
    echo "$1 n'est pas un repertoire..."
    exit 1
fi

############################################

IFS='
'
for i in `ls -1 "$1"`
do
    echo "Fichier : $i"
    NAME=`echo "$i" | cut -d '_' -f2 | cut -d '.' -f1`
    sed -i "1s/$/$NAME/" "$1/$i"
done
#./reco.sh
