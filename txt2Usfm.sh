#!/bin/bash
git clone https://bitbucket.org/clementinetextproject/text.git VulgSearch
rm -r usfm/
cp -r VulgSearch/ usfm/
cd usfm/
find -type f \( -name "*.lat" -o -name "*.lat" \) | while read f
do
	
	echo "Convert $f to utf-8..."
	
	cp -p "$f" "$f.original"
	iconv -f WINDOWS-1252 -t UTF-8 "$f.original" > "$f";
done
find -type f -name "*.original" -delete

sed -i 's/\\/\n\\p /g' *.lat
##Correction des lettres hébraïques dans Lam
sed -ri 's/(3:[0-9]* )<*.*>/\1/g' Lam.lat
sed -ri 's/(3:1 )/\1<Aleph>/g' Lam.lat
sed -ri 's/(3:4 )/\1<Beth>/g' Lam.lat
sed -ri 's/(3:7 )/\1<Ghimel>/g' Lam.lat
sed -ri 's/(3:10 )/\1<Daleth>/g' Lam.lat
sed -ri 's/(3:13 )/\1<He>/g' Lam.lat
sed -ri 's/(3:16 )/\1<Vau>/g' Lam.lat
sed -ri 's/(3:19 )/\1<Zain>/g' Lam.lat
sed -ri 's/(3:22 )/\1<Heth>/g' Lam.lat
sed -ri 's/(3:25 )/\1<Teth>/g' Lam.lat
sed -ri 's/(3:28 )/\1<Jod>/g' Lam.lat
sed -ri 's/(3:31 )/\1<Caph>/g' Lam.lat
sed -ri 's/(3:34 )/\1<Lamed>/g' Lam.lat
sed -ri 's/(3:37 )/\1<Mem>/g' Lam.lat
sed -ri 's/(3:40 )/\1<Nun>/g' Lam.lat
sed -ri 's/(3:43 )/\1<Samech>/g' Lam.lat
sed -ri 's/(3:46 )/\1<Phe>/g' Lam.lat
sed -ri 's/(3:49 )/\1<Ain>/g' Lam.lat
sed -ri 's/(3:52 )/\1<Sade>/g' Lam.lat
sed -ri 's/(3:55 )/\1<Coph>/g' Lam.lat
sed -ri 's/(3:58 )/\1<Res>/g' Lam.lat
sed -ri 's/(3:61 )/\1<Sin>/g' Lam.lat
sed -ri 's/(3:64 )/\1<Thau>/g' Lam.lat

##Ajout \id à la première ligne
sed -i '1i\\\id ' *.lat
#Remplacer chiffre début suivt de chiffre et 1. Par \c et \v 1
sed -ri 's/^([0-9]*):(1) /\\c \1\n\\p\n\\v \2 /g' *.lat

sed -i 's/\[//g' *.lat
sed -i 's/\]//g' *.lat
sed -i 's#/#\n\\q#g' *.lat
#Suppression des premier caractère de la ligne et remplacement par \v
sed -ri 's/^[0-9]*:([0-9]*) /\\v \1 /g' *.lat
sed -ri 's#(\\v [0-9]* )<(Chorus|Sponsa|Sponsus|Chorus Adolescentularum)>#\n\\sp \2\n\\q\n\1#g' *.lat
sed -ri 's#(\\v [0-9]* )<(Prologus)>#\n\\is \2\n\\ip \n\1#g' *.lat
sed -ri 's/(\\v [0-9]* )<(.*)>/\\qa \2\n\\q\n\1/g' *.lat
sed -ri ':a;N;$!ba;s/\\q\n(\\qa)/\1/g' *.lat
sed -i '/^$/d' *.lat
sed -ri 's/(\\v 1 )(Et factum est*.*|Alleluja\. )<(Aleph)>/\2\n\\qa \3\n\\q\n\1/g' *.lat
sed -ri 's/(\\v 1 )(Multorum nobis*.*)(Omnis)/\2\n\\p\n\1\3/g' *.lat
rename 's/1Cor\.lat/1CO\.lat/g' *.lat
rename 's/1Jo\.lat/1JN\.lat/g' *.lat
rename 's/1Mcc\.lat/1MA\.lat/g' *.lat
rename 's/1Par\.lat/1CH\.lat/g' *.lat
rename 's/1Ptr\.lat/1PE\.lat/g' *.lat
rename 's/1Rg\.lat/1SA\.lat/g' *.lat
rename 's/1Thes\.lat/1TH\.lat/g' *.lat
rename 's/1Tim\.lat/1TI\.lat/g' *.lat
rename 's/2Cor\.lat/2CO\.lat/g' *.lat
rename 's/2Jo\.lat/2JN\.lat/g' *.lat
rename 's/2Mcc\.lat/2MA\.lat/g' *.lat
rename 's/2Par\.lat/2CH\.lat/g' *.lat
rename 's/2Ptr\.lat/2PE\.lat/g' *.lat
rename 's/2Rg\.lat/2SA\.lat/g' *.lat
rename 's/2Thes\.lat/2TH\.lat/g' *.lat
rename 's/2Tim\.lat/2TI\.lat/g' *.lat
rename 's/3Jo\.lat/3JN\.lat/g' *.lat
rename 's/3Rg\.lat/1KI\.lat/g' *.lat
rename 's/4Rg\.lat/2KI\.lat/g' *.lat
rename 's/Abd\.lat/OBA\.lat/g' *.lat
rename 's/Act\.lat/ACT\.lat/g' *.lat
rename 's/Agg\.lat/HAG\.lat/g' *.lat
rename 's/Am\.lat/AMO\.lat/g' *.lat
rename 's/Apc\.lat/REV\.lat/g' *.lat
rename 's/Bar\.lat/BAR\.lat/g' *.lat
rename 's/Col\.lat/COL\.lat/g' *.lat
rename 's/Ct\.lat/SNG\.lat/g' *.lat
rename 's/Dn\.lat/DAN\.lat/g' *.lat
rename 's/Dt\.lat/DEU\.lat/g' *.lat
rename 's/Ecl\.lat/ECC\.lat/g' *.lat
rename 's/Eph\.lat/EPH\.lat/g' *.lat
rename 's/Esr\.lat/EZR\.lat/g' *.lat
rename 's/Est\.lat/EST\.lat/g' *.lat
rename 's/Ex\.lat/EXO\.lat/g' *.lat
rename 's/Ez\.lat/EZK\.lat/g' *.lat
rename 's/Gal\.lat/GAL\.lat/g' *.lat
rename 's/Gn\.lat/GEN\.lat/g' *.lat
rename 's/Hab\.lat/HAB\.lat/g' *.lat
rename 's/Hbr\.lat/HEB\.lat/g' *.lat
rename 's/Is\.lat/ISA\.lat/g' *.lat
rename 's/Jac\.lat/JAS\.lat/g' *.lat
rename 's/Jdc\.lat/JDG\.lat/g' *.lat
rename 's/Jdt\.lat/JDT\.lat/g' *.lat
rename 's/Job\.lat/JOB\.lat/g' *.lat
rename 's/Joel\.lat/JOL\.lat/g' *.lat
rename 's/Jo\.lat/JHN\.lat/g' *.lat
rename 's/Jon\.lat/JON\.lat/g' *.lat
rename 's/Jos\.lat/JOS\.lat/g' *.lat
rename 's/Jr\.lat/JER\.lat/g' *.lat
rename 's/Jud\.lat/JUD\.lat/g' *.lat
rename 's/Lam\.lat/LAM\.lat/g' *.lat
rename 's/Lc\.lat/LUK\.lat/g' *.lat
rename 's/Lv\.lat/LEV\.lat/g' *.lat
rename 's/Mal\.lat/MAL\.lat/g' *.lat
rename 's/Mch\.lat/MIC\.lat/g' *.lat
rename 's/Mc\.lat/MRK\.lat/g' *.lat
rename 's/Mt\.lat/MAT\.lat/g' *.lat
rename 's/Nah\.lat/NAM\.lat/g' *.lat
rename 's/Neh\.lat/NEH\.lat/g' *.lat
rename 's/Nm\.lat/NUM\.lat/g' *.lat
rename 's/Os\.lat/HOS\.lat/g' *.lat
rename 's/Phlm\.lat/PHM\.lat/g' *.lat
rename 's/Phlp\.lat/PHP\.lat/g' *.lat
rename 's/Pr\.lat/PRO\.lat/g' *.lat
rename 's/Ps\.lat/PSA\.lat/g' *.lat
rename 's/Rom\.lat/ROM\.lat/g' *.lat
rename 's/Rt\.lat/RUT\.lat/g' *.lat
rename 's/Sap\.lat/WIS\.lat/g' *.lat
rename 's/Sir\.lat/SIR\.lat/g' *.lat
rename 's/Soph\.lat/ZEP\.lat/g' *.lat
rename 's/Tit\.lat/TIT\.lat/g' *.lat
rename 's/Tob\.lat/TOB\.lat/g' *.lat
rename 's/Zach\.lat/ZEC\.lat/g' *.lat
rename 's/\.lat/\.usfm/g' *.lat

cd ../
./nom.sh ./usfm

u2o.py -e utf-8 -l la -o vulgclementine.osis.xml -v -d -x VulgClementine usfm/*.usfm
mkdir ~/.sword/modules/texts/ztext/vulgclementinenv/
osis2mod ~/.sword/modules/texts/ztext/vulgclementinenv/ vulgclementine.osis.xml -v Vulg -z
